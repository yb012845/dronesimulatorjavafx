package application;

/**
 * State is an enum for determining the state of a drone. Whether it is dead or
 * alive, rather than using integers
 * 
 * @author joshh
 *
 */
public enum State {
	Alive, Dead
}
